package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // A test to check that the boxComplete function performs correctly 
    @Test
    public void testBoxComplete() {

        // Creates a grid with a box starting at (0,0)
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(15, 8, 2);
        grid.drawHorizontal(0, 0, 1);
        grid.drawHorizontal(0, 1, 1);
        grid.drawVertical(0, 0, 1);
        grid.drawVertical(1, 0, 1);

        logger.info("This test checks that the boxComplete function works correctly");

        assertAll(
            "A complete box must return true, an incomplete must return false.",
            () -> assertTrue(grid.boxComplete(0,0)),
            // Returns false as the x,y co-ordinates passed are not the left and top co-ordinates of the box
            () -> assertFalse(grid.boxComplete(1,1))
        );
    }
    
    // A test to check that the program throws an exception when a player attempts to draw a line in a position that
    // has already been played
    @Test
    public void testDrawLine() {

        // Creates a grid and draws a horizontal and vertical line at (0,0)
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(15, 8, 2);
        grid.drawHorizontal(0, 0, 1);
        grid.drawVertical(0, 0, 1);

        logger.info("This test checks that an exception is thrown when a player tries to draw a line that is already drawn");

        assertThrows(IllegalStateException.class, () -> grid.drawHorizontal(0, 0, 1), "The horizontal line was drawn more than once");
        assertThrows(IllegalStateException.class, () -> grid.drawVertical(0, 0, 1), "The vertical line was drawn more than once");
    }
}
